Visual Studio 2019 .NET Core 3.1 Console Application in C#

# Code Challenge

You are given two strings.  Write a program that prints "true" if both the words have same character sequence and prints "false" if they don't have the same sequence.  For example, the word "worldwar" has the same sequence as "arWorldW". (ignore case)

Input  
Input will have s1 and s2 separated into two lines

Output  
Please output "true" or "false" based on whether it's a rotation or not.  
Code evaluation is based on output, please do NOT print anything else.


# Notes

On an initial run I focused on the top line instruction talking about "same character sequence", only having 1 test case to work against.  
The initial solution passed 6 of 10 hidden tests, but I was unaware of results until a few days later and needed to pass 7 of 10.  
Later discussion pointed to the Output comments, if the character sequence is a "rotation" or not.  
Created a batch of 10 test cases with an added solution that should work.  If you run across a case that does not work, let me know so we can update the logic behind it.  Thanks!  

Good luck with practice!
