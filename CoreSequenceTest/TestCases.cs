using NUnit.Framework;

namespace CoreSequenceTest
{
    public class Tests
    {
        /// <summary>
        /// 2 Solutions are provided, replace "codeHere" in "Program.cs" from each Solution for testing
        /// </summary>
        
        [Test]
        public void Test1()
        {
            // Same character sequence, but rotated, result should be True
            string testString = "worldwar\r\narWorldW";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("true", result.ToLower());
        }

        [Test]
        public void Test2()
        {
            // Same characters, different sequences, result should be False
            string testString = "AAbbAAbbAAbb\r\nabABabABabAB";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

        [Test]
        public void Test3()
        {
            // No strings to compare, result should be False
            string testString = "\r\n";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

        [Test]
        public void Test4()
        {
            // Character sequence match, but rotated, result should be True
            string testString = " Code Test\r\nTest Code ";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("true", result.ToLower());
        }

        [Test]
        public void Test5()
        {
            // Character sequence match, but rotated, result should be True
            string testString = "challenges fun they are \r\nAre Challenges Fun They ";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("true", result.ToLower());
        }

        [Test]
        public void Test6()
        {
            // Character sequence match, but rotated, result should be True
            string testString = " CoNgRaTs  WhEn   DoNe\r\nwhen   done congrats  ";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("true", result.ToLower());
        }

        [Test]
        public void Test7()
        {
            // No data, result should be False
            string testString = "";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

        [Test]
        public void Test8()
        {
            // More than 2 strings to compare, result should be False
            string testString = "abc\r\nbca\r\ncab";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

        [Test]
        public void Test9()
        {
            // Same character sequence, not rotated, result should be False
            string testString = "ThisIsNotRotated\r\nthisisnotrotated";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

        [Test]
        public void Test10()
        {
            // Same character sequence, flipped, result should be False
            string testString = "ThisIsFlipped\r\ndeppilfsisiht";
            string result = CoreSequence.Program.codeHere(testString);
            Assert.AreEqual("false", result.ToLower());
        }

    }
}