﻿using System;

namespace CoreSequence
{
    class Solution1
    {
        /// <summary>
        /// The original Challenge Test solution compared character sequence between 2 strings, passed 6 of 10 test cases
        /// Discussion with support team said "true" / "false" is based on the character sequence being rotated
        /// The solution below is a revised design that *should* pass the test, but no way to verify
        /// </summary>
        
        static void Solution1_Main(string[] args)
        {
            string testCase = "worldwar\r\narWorldW";

            Console.WriteLine(codeHere(testCase));
        }

        public static string codeHere(String inputData)
        {
            // No data?  Rotated = False
            if (inputData.Trim().Length == 0)
            {
                return "false";
            }

            var rowData = inputData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            // Validate we have 2 strings to process, otherwise Rotated = False
            if (rowData.Length != 2)
            {
                return "false";
            }

            // Validate if content is rotated
            return IsRotated(rowData[0], rowData[1]).ToString();
        }

        static bool IsRotated(string s1, string s2)
        {
            // String comparison doesn't match?  Rotated = False
            if (s1.Length != s2.Length)
                return false;

            // Get Index of string 2 in relation to string 1 content
            int indexOf = s1.IndexOf(s2, StringComparison.OrdinalIgnoreCase);

            // Same text, not rotated
            if (indexOf == 0)
            {
                return false;
            }

            // Get Index of string 2 in relation to string 1 doubled up to see if character sequence has been rotated
            indexOf = (s1 + s1).IndexOf(s2, StringComparison.OrdinalIgnoreCase);

            // Same text, but rotated
            if (indexOf > -1)
                return true;

            return false;
        }
    }
}
