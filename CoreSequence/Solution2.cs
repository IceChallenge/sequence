﻿using System;

namespace CoreSequence
{
    class Solution2
    {
        /// <summary>
        /// This Challenge Test surfaced again the next time the system was used, the solution below passed 9 of 10 test cases
        /// 
        /// The test case that did not pass was debated with support staff and a ticket was submitted to address the challenge
        /// The test case was based on 2 strings, each 2 characters, both numeric and the same number.  ie. 11 / 11
        /// The result of the logic below returns "false" as they are not rotated per the instruction in Output
        /// The support team argued the result should be "true" as they are rotated, but you can't make that distinguishment based on data
        /// </summary>
        
        static void Solution2_Main(string[] args)
        {
            string testCase = "worldwar\r\narWorldW";

            Console.WriteLine(codeHere(testCase));
        }

        public static string codeHere(String inputData)
        {
            // Default Input Test Case
            // coke
            // ecok

            // No data?  ..return "false"
            if (inputData.Trim() == "")
            {
                // Console.WriteLine("no data?");
                return "false";
            }

            var dataRows = inputData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            // Expecting 2 Strings, if not provided ..return "false"
            if (dataRows.Length != 2)
            {
                // Console.WriteLine("2 strings not provided?");
                return "false";
            }

            // If String lengths are not equal, full rotation can not be validated ..return "false"
            if (dataRows[0].Length != dataRows[1].Length)
            {
                // Console.WriteLine("2 strings not of same length?");
                return "false";
            }

            // No data to compare? ..return "false"
            if (dataRows[0].Trim() == "" || dataRows[1].Trim() == "")
            {
                // Console.WriteLine("No data to compare in 1 of 2 strings?");
                return "false";
            }

            int indexOf = dataRows[0].IndexOf(dataRows[1], StringComparison.OrdinalIgnoreCase);

            if (indexOf == 0)
            {
                // Text matches, same character sequence, not rotated ..return "false"
                return "false";
            }

            string comparison1 = dataRows[0] + dataRows[0];
            string comparison2 = dataRows[1];

            if (comparison1.IndexOf(comparison2, StringComparison.OrdinalIgnoreCase) > -1)
            {
                // Rotation found

                // Console.WriteLine("Comparison1: " + comparison1);
                // Console.WriteLine("Comparison2: " + comparison2);
                // Console.WriteLine("Same text, rotated");
                return "true";
            }

            // If rotation not identified ..return "false"
            return "false";
        }
    }
}
