﻿using System;

namespace CoreSequence
{
    public class Program
    {
        /*
        
        Instructions:

            You are given two strings.  Write a program that prints "true" if both the words 
            have same character sequence and prints "false" if they don't have the same sequence.  
            For example, the word "worldwar" has the same sequence as "arWorldW". (ignore case)

            Input  
            Input will have s1 and s2 separated into two lines

            Output  
            Please output "true" or "false" based on whether it's a rotation or not.  
            Code evaluation is based on output, please do NOT print anything else.

         */

        static void Main(string[] args)
        {
            string testCase = "worldwar\r\narWorldW";

            Console.WriteLine(codeHere(testCase));
        }

        public static string codeHere(string inputData)
        {
            // Use this function to return your solution.

            return "false";
        }

    }
}
